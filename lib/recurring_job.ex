defmodule RecurringJob do
  alias RecurringJob.Job

  def start do
    Job.start_link()
  end
end
