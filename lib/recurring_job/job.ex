defmodule RecurringJob.Job do
  use GenServer

  @url "https://box.maoyan.com/promovie/api/box/second.json"

  def start_link() do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    schedule_fetch(0)
    {:ok, %{}}
  end

  def handle_info({:fetch}, state) do
    fetch_box() |> print
    schedule_fetch(1_000)
    {:noreply, state}
  end

  defp fetch_box() do
    @url
    |> HTTPoison.get!()
    |> Map.get(:body)
    |> Jason.decode!()
    |> Map.get("data")
    |> Map.take(["totalBox", "updateInfo"])
  end

  defp print(%{"totalBox" => totalBox, "updateInfo" => updateInfo})  do
    IO.puts "\n今日票房：#{totalBox}万，#{updateInfo} "
    IO.puts "======================================"
  end

  def schedule_fetch(interval) do
    Process.send_after(self(), {:fetch}, interval)
  end
end
